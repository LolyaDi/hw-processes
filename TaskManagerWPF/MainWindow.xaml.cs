﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Management;
using System.Windows;

namespace TaskManagerWPF
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            var processes = Process.GetProcesses();

            foreach (var process in processes)
            {
                processesDataGrid.Items.Add(new
                {
                    process.ProcessName,
                    process.Id,
                    UserName = GetUserName(process.Id),
                    Status = (process.Responding == true) ? "Responding" : "Not responding",
                    WorkingSet64 = (process.WorkingSet64 / 1024).ToString() + "K",
                    PeakWorkingSet64 = (process.PeakWorkingSet64 / 1024).ToString() + "K"
                });
            }
        }

        private string GetUserName(int processId)
        {
            string query = "Select * From Win32_Process Where ProcessID = " + processId;
            ManagementObjectSearcher searcher = new ManagementObjectSearcher(query);
            ManagementObjectCollection processList = searcher.Get();

            string userName = "Unknown";

            foreach (ManagementObject managmentObject in processList)
            {
                string[] argumentList = new string[] { string.Empty, string.Empty };
                int returnValue = Convert.ToInt32(managmentObject.InvokeMethod("GetOwner", argumentList));
                if (returnValue == 0)
                {
                    userName = argumentList[0];
                }
            }

            return userName;
        }

        private void KillButtonClick(object sender, RoutedEventArgs e)
        {
            dynamic selectedProcess = processesDataGrid.SelectedItem;

            if (selectedProcess != null)
            {
                try
                {
                    foreach (var process in Process.GetProcessesByName(selectedProcess.ProcessName))
                    {
                        process.Kill();
                        process.WaitForExit();
                    }

                    processesDataGrid.Items.RemoveAt(processesDataGrid.SelectedIndex);
                }
                catch (Win32Exception)
                {
                    MessageBox.Show("Can't kill the process!");
                }
                catch
                {
                    MessageBox.Show("Unpredictable error!");
                }
            }
            else
            {
                MessageBox.Show("Process isn't selected!");
            }
        }
    }
}
